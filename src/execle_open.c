/* Copyright (C) 2001 Red Hat, Inc.
   Written by Jakub Jelinek <jakub@redhat.com>, 2001.

* SPDX-License-Identifier: GPL-2.0-or-later
*/

#include <config.h>
#include <errno.h>
#include <error.h>
#include <stdio.h>
#include <sys/wait.h>
#include <unistd.h>

static pid_t pid;

int
execve_close (FILE *f)
{
  pid_t p;
  int status;

  if (f != NULL)
    fclose (f);
  while ((p = waitpid (pid, &status, 0)) == -1 && errno == EINTR);
  if (p == -1 || ! WIFEXITED (status))
    return -1;
  return WEXITSTATUS (status);
}

FILE *
execve_open (const char *path, char *const argv[], char *const envp[])
{
  int p[2];
  FILE *f;

  if (pipe (p) < 0)
    {
      error (0, errno, "Could not run %s", path);
      return NULL;
    }

  switch (pid = vfork ())
    {
    case -1:
      error (0, errno, "Could not run %s", path);
      return NULL;
    case 0:
      close (p[0]);
      if (p[1] != 1)
	{
	  dup2 (p[1], 1);
	  close (p[1]);
	}
      dup2 (1, 2);
      execve (path, argv, envp);
      error (0, errno, "Could not run %s", path);
      _exit (127);
    }

  close (p[1]);

  f = fdopen (p[0], "r");
  if (f == NULL)
    {
      close (p[0]);
      execve_close (NULL);
    }

  return f;
}
