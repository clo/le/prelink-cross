/* Copyright (C) 2001, 2004, 2006 Red Hat, Inc.
   Written by Jakub Jelinek <jakub@redhat.com>, 2001.

* SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef LAYOUT_H
#define LAYOUT_H

struct layout_libs
  {
    struct prelink_entry **libs;
    struct prelink_entry **binlibs;
    struct prelink_entry *list;
    struct prelink_entry *fake;
    GElf_Addr mmap_base, mmap_start, mmap_fin, mmap_end, max_page_size;
    void *arch_data;
    int flags;
    int nlibs;
    int nbinlibs;
    int fakecnt;
  };

#endif /* LAYOUT_H */
