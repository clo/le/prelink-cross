/* Copyright (C) 2001 Red Hat, Inc.
   Written by Jakub Jelinek <jakub@redhat.com>, 2001.

* SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef PRELINKTAB_H
#define PRELINKTAB_H

#include "hashtab.h"
#include "prelink.h"

extern htab_t prelink_devino_htab, prelink_filename_htab;
extern int prelink_entry_count;

#endif /* PRELINKTAB_H */
