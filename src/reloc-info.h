/* Copyright (C) 2008 CodeSourcery
   Written by Maciej W. Rozycki <macro@codesourcery.com>, 2008.

* SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef RELOC_INFO_H
#define RELOC_INFO_H

#include "prelink.h"

/* Reloc info primitives.  */
GElf_Xword reloc_r_sym (DSO *dso, GElf_Xword r_info);
GElf_Xword reloc_r_ssym (DSO *dso, GElf_Xword r_info);
GElf_Xword reloc_r_type (DSO *dso, GElf_Xword r_info);
GElf_Xword reloc_r_type2 (DSO *dso, GElf_Xword r_info);
GElf_Xword reloc_r_type3 (DSO *dso, GElf_Xword r_info);

GElf_Xword reloc_r_info (DSO *dso, GElf_Word r_sym, GElf_Word r_type);
GElf_Xword reloc_r_info_ext (DSO *dso, GElf_Word r_sym, Elf64_Byte r_ssym,
			     Elf64_Byte r_type, Elf64_Byte r_type2,
			     Elf64_Byte r_type3);

#endif /* RELOC_INFO_H */
