/* Copyright (C) 2001, 2002 Red Hat, Inc.
   Written by Jakub Jelinek <jakub@redhat.com>, 2001.

* SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef RELOC_H
#define RELOC_H

#include "prelink.h"

struct reloc_info
{
  int first; /* First dynamic SHT_REL* section.  */
  int last; /* Last dynamic SHT_REL* section not counting .rel*.plt.  */
  int plt; /* .rel*.plt section.  */
  int overlap; /* 1 if DT_REL{,A}SZ range includes DT_PLTRELSZ range.  */
  int reldyn_rela; /* first..last sections were originally RELA.  */
  int plt_rela; /* plt section was originally RELA.  */
  int rel_to_rela; /* first..last sections have to be converted REL->RELA.  */
  int rel_to_rela_plt; /* plt section has to be converted REL->RELA.  */
  int relcount; /* DT_RELCOUNT resp. DT_RELACOUNT.  */
};

int find_reloc_sections (DSO *dso, struct reloc_info *rinfo);
int convert_rel_to_rela (DSO *dso, int i);
int convert_rela_to_rel (DSO *dso, int i);
int update_dynamic_rel (DSO *dso, struct reloc_info *rinfo);
int undo_sections (DSO *dso, int undo, struct section_move *move,
		   struct reloc_info *rinfo, GElf_Ehdr *ehdr,
		   GElf_Phdr *phdr, GElf_Shdr *shdr);

#endif /* RELOC_H */
