/* Copyright (C) 2001, 2004 Red Hat, Inc.
   Written by Jakub Jelinek <jakub@redhat.com>, 2001.

* SPDX-License-Identifier: GPL-2.0-or-later
*/

#ifndef SPACE_H
#define SPACE_H

struct readonly_adjust
{
  off_t basemove_adjust;
  GElf_Addr basemove_end;
  int moveend;
  int move2;
  int newcount, *new;
  struct section_move *move;
};

void insert_readonly_section (GElf_Ehdr *ehdr, GElf_Shdr *shdr, int n,
			      struct readonly_adjust *adjust);
int remove_readonly_section (GElf_Ehdr *ehdr, GElf_Shdr *shdr, int n,
			     struct readonly_adjust *adjust);
int find_readonly_space (DSO *dso, GElf_Shdr *add, GElf_Ehdr *ehdr,
			 GElf_Phdr *phdr, GElf_Shdr *shdr,
			 struct readonly_adjust *adjust);

#endif /* SPACE_H */
